class Fixnum
  def in_words
    return "zero" if zero?
    chunks = break_into_three_digit_chunks

    words = chunks.map do |chunk|
      wordify_three_digits(chunk)
    end

    words_with_orders = add_orders_of_magnitude(words)

    words_with_orders.reject(&:empty?).join(' ')
  end

  private

  DIGITS_IN_WORDS = {
    '0' => '',
    '1' => 'one',
    '2' => 'two',
    '3' => 'three',
    '4' => 'four',
    '5' => 'five',
    '6' => 'six',
    '7' => 'seven',
    '8' => 'eight',
    '9' => 'nine'
  }

  TENS_DIGITS_IN_WORDS = {
    '2' => 'twenty',
    '3' => 'thirty',
    '4' => 'forty',
    '5' => 'fifty',
    '6' => 'sixty',
    '7' => 'seventy',
    '8' => 'eighty',
    '9' => 'ninety'
  }

  TEENS_IN_WORDS = {
    '10' => 'ten',
    '11' => 'eleven',
    '12' => 'twelve',
    '13' => 'thirteen',
    '14' => 'fourteen',
    '15' => 'fifteen',
    '16' => 'sixteen',
    '17' => 'seventeen',
    '18' => 'eighteen',
    '19' => 'nineteen'
  }

  ORDERS_OF_MAGNITUDE = {
    1 => '',
    2 => 'thousand',
    3 => 'million',
    4 => 'billion',
    5 => 'trillion'
  }

  def break_into_three_digit_chunks
    digits = to_s.chars

    chunks = []

    chunks.unshift(digits.pop(3).join) until digits.empty?
    chunks[0] = pad_chunk(chunks[0])

    chunks
  end

  def pad_chunk(number_string)
    number_string = '0' + number_string until number_string.length == 3

    number_string
  end

  def wordify_digit(digit)
    DIGITS_IN_WORDS[digit]
  end

  def wordify_two_digits(digits)
    tens, ones = digits[0], digits[1]

    return wordify_digit(ones) if tens == '0'
    return wordify_teens(digits) if tens == '1'

    "#{TENS_DIGITS_IN_WORDS[tens]} #{DIGITS_IN_WORDS[ones]}"
  end

  def wordify_three_digits(digits)
    hundreds = wordify_hundreds(digits[0])
    tens_and_ones = wordify_two_digits(digits[1, 2])

    (hundreds + tens_and_ones).strip
  end

  def wordify_hundreds(digit)
    return '' if digit == '0'

    "#{DIGITS_IN_WORDS[digit]} hundred "
  end

  def wordify_teens(digits)
    TEENS_IN_WORDS[digits]
  end

  def add_orders_of_magnitude(words)
    words.map.with_index do |word, idx|
      if word.empty?
        word
      else
        "#{word} #{ORDERS_OF_MAGNITUDE[words.size - idx]}".strip
      end
    end
  end
end
